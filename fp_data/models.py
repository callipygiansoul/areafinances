from django.db import models

# Create your models here.



class Account(models.Model):
    accountTypes = [
        ('expense', 'expense'),
        ('income', 'income'),
        ('asset', 'asset'),
    ]

    shortName = models.CharField(max_length=255, unique=True)
    displayName = models.CharField(max_length=255, unique=True)
    accountType = models.CharField(max_length=255, choices=accountTypes)
    aggregator = models.ForeignKey('Aggregator', related_name='aggregator', blank=True, null=True)

    def __str__(self):
        return self.displayName


class AccountAlias(models.Model):
    alias = models.CharField(max_length=255, unique=True)
    account = models.ForeignKey('Account', related_name='alias')
    
    def __str__(self):
        return self.alias

class SkippedLabel(models.Model):
    label = models.CharField(max_length=255, unique=True)
    
    def __str__(self):
        return self.label



class Ledger(models.Model):
    account = models.ForeignKey('Account', related_name='account')
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    quarter = models.IntegerField()
    year = models.IntegerField()

    def __str__(self):
        return self.account


class Aggregator(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name
