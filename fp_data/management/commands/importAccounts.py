from django.core.management.base import BaseCommand
from fp_data.models import Account, AccountAlias, Aggregator
import xlrd


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('spreadsheet')

    def handle(self, *args, **options):
        reader = xlrd.open_workbook(filename=options['spreadsheet'])
        sheet = reader.sheet_by_index(0)
        header = sheet.row_values(0)
        print('header', header)
        for i in range(1,sheet.nrows):
            row = dict(zip(header, sheet.row(i)))
            print('row', row)
            print('aggdir', row['aggregator'].ctype)
            if row['aggregator'].ctype not in (0, 6):
                aggList = Aggregator.objects.values_list('name', flat=True)
                print('rowval={}, agglist={}'.format(row['aggregator'].value, aggList))
                try:
                    agg = Aggregator.objects.get(name=row['aggregator'].value) 
                except:
                     agg = Aggregator(name=row['aggregator'].value)
                     agg.save()

            acct = Account(shortName=row['shortName'].value, displayName=row['displayName'].value, accountType=row['acctType'].value)
            if row['aggregator'].ctype not in (0, 6):
                acct.aggregator = agg
            acct.save()

            alias = AccountAlias(alias=row['alias'].value, account=acct)
            alias.save()




            




#shortName   displayName acctType    alias   aggregator