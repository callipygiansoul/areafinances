from django.core.management.base import BaseCommand
from django.db import transaction
import xlrd
import re
import sys
from fp_data.models import SkippedLabel, Account, AccountAlias, Aggregator
import string

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('workbook')
        parser.add_argument('--sheet', help='(optional) worksheet to use. '
            'Overrides default behavior, which search for "Q4" then "Sheet1".')

    def handle(self, *args, **options):
        reader = xlrd.open_workbook(filename=options['workbook'])
        if options['sheet']:
            filters = [options['sheet']]
        else:
            filters = ['Q4', 'Sheet1']

        # print('filters', filters)
        # print('reader snames=', reader.sheet_names())

        filteredSheets = [sheetname for sheetname in reader.sheet_names() if sheetname in filters]

        if len(filteredSheets) > 1:
            print('Too many sheets were found that meet the filter criteria.  Consider using the --sheet option.')
            print('Sheets found:  {}'.filter(', '.join(filteredSheets)))
        if len(filteredSheets) == 0:
            print("Couldn't find any worksheets that matched with the filter.")
            exit()

        # print('sheets=', reader.sheet_names())

        try:
            sheet = reader.sheet_by_name(filteredSheets[0])
        except AttributeError:
            if options['sheet']:
                print("The workbook doesn't have a sheet named {}.  Exiting".format(options['sheet']))
            else:
                print("Couldn't find a sheet with either Q4 or Sheet1 in the name.  Use the --sheet option to specify the name of the sheet")

        if 'YE' in options['workbook']:
            print('YE')
        else:
            coords, labelCol = self.mapSheet(sheet, 'quarter')
            # print('coords, labelCol', coords, labelCol)

            skipThese = SkippedLabel.objects.values_list('label', flat=True)
            accountList = Account.objects.values_list('shortName', flat=True)
            aliasList = AccountAlias.objects.values_list('alias', flat=True)
            # print('skipthese', skipThese)
            

            for nrow in range(sheet.nrows):
                cellType = sheet.cell(nrow, labelCol).ctype
                cellValue = sheet.cell(nrow, labelCol).value.lower()

                if (cellType not in (0, 6)) and \
                    (cellValue not in skipThese):

                    self.evalAccounts(cellValue, aliasList, accountList)
                    #print('cv', cellValue)
                    #unsavedObjects.append(self.evalAccount(cellValue, aliasList, accountList))
                    #print('obj=', unsavedObjects)
                    print('')

    
    def evalAccounts(self, label, aliases, accounts):

        label = label.strip().strip(string.punctuation)
        if label in aliases:
            return True
        while True:
            print("The label \"{}\" isn't in the list of accounts. How would you like to handle this?".format(label))
            print("1: Skip lables like this in the future")
            print("2: Add the label as an alias for an existing account")
            print("3: Add the account to the accepted account list with the label as an alias")
            choice = input("Type 1, 2, or 3 and hit enter: ")
            while choice not in ('1', '2', '3'):
                choice = input("You must enter 1, 2, or 3.  Try again: ")
            print('')
            if choice == '1':
                skippedObj = SkippedLabel(label=label)
                skippedObj.save()
                break
            elif choice == '2':
                newAliasObj = self.addAlias(label, accounts)
                if newAliasObj != 'exit':
                    newAliasObj.save()
                    break
            elif choice == '3':
                testVal = self.addAccount(label)
                if testVal != 'exit':
                    break

            else:
                print("You may ask yourself, \"How did I get here\".  I'm asking the same thing, it shouldn't have been possible.  Dying.")
                sys.exit()


            

    def addAlias(self, newAlias, accounts):

        inp = input('Do you want to print a list of existing accounts? (y/n): ')
        if inp in ('y', 'yes'):
            print('\n'.join(accounts))
            print('\nJust a reminder, the new alias name is "{}"'.format(newAlias))
        inp = input('Enter the short name of the account you want to associate the ' \
            'alias with (or type "exit" to return to the prior menu): ')
        if inp == 'exit':
            return 'exit'
        while inp not in Account.objects.values_list('shortName', flat=True):
            inp = input("What you enetered doesn't match an existing account, try again: ")

        return AccountAlias(alias=newAlias, account=Account.objects.get(shortName=inp))


    def addAccount(self, newAlias):
        acceptedChars = set(string.ascii_letters+string.digits+'-_')
        inp = input('Do you want to print a list of existing accounts? (y/n): ')
        if inp in ('y', 'yes'):
            print('\n'.join(accounts))
            print('\nJust a reminder, the new alias name is "{}"'.format(newAlias))
        inp = input('\nEnter the short name of the account you want to create. ' \
            'Just letters, numbers, dashes, and underscores please. Note that ' \
            'the name will automatically be converted to lower case \n' \
            'Or type "exit" to return to the prior menu: ')
        if inp == 'exit':
            return 'exit'
        while True:
            if set(inp) > acceptedChars:
                inp = input("The account name contains disallowed characters, try again: ")
            elif not inp:
                inp = input("You must enter an account name, try again: ")
            else:
                break
        acctName = inp

        choices = [x[0] for x in Account.accountTypes]
        print('\nWhat type of account is it?  These are your choices:')
        print('\n'.join(choices))
        inp = input("Select an account type: ")
        while True:
            if inp not in choices:
                inp = input("You've selected an invalid account type.  Pick again: ")
            else:
                break
        acctType = inp

        acctAgg = None
        aggs = list(Aggregator.objects.values_list('name', flat=True))
        print("\nThese are you aggregator choices:\n{}".format(', '.join(aggs)))
        inp = input('\nIf you want to associate an aggreagor, enter it here.  If not, just hit enter: ')
        if inp != '':
            while True:
                if inp not in aggs:
                    inp = input("You've selected an invalid aggregator.  Pick again: ")
                else:
                    break
        

        displayName = ''
        inp = input("By default, the label will be used as the display name.  " \
            "If you'd rather customize the display name, enter it here and the " \
            "label will be used as an alias (just hit enter to use the label): ")
        if inp == '':
            displayName = newAlias
        else:
            displayName = inp

        acctObj =  Account(
            shortName=acctName,
            displayName=displayName,
            accountType=acctType,
            aggregator=acctAgg
        )
        acctObj.save()
        aliasObj = AccountAlias(alias=newAlias, account=acctObj)
        aliasObj.save()

        return True


                    

    def mapSheet(self, sheet, mapType):
        coords = {}
        labels = None
        #print('test', sheet.cell(17,0))
        if mapType.lower() == 'quarter':
            for nrow in range(0, 20):
                for ncol in range(len(sheet.row(nrow))):
                    cell = sheet.cell(nrow, ncol)
                    #print('cell=', cell.value)
                    match = re.search(r'Q.*([1234]{1})', str(cell.value), flags=re.IGNORECASE)

                    if match:
                        coords['Q'+match.group(1)] = ncol

                    if 'delegate' in str(cell.value).lower():
                        labels = ncol


            return coords, labels