from django.contrib import admin

# Register your models here.

from .models import Account, AccountAlias, Ledger, Aggregator, SkippedLabel

admin.site.register([Account, AccountAlias, Ledger, Aggregator, SkippedLabel])