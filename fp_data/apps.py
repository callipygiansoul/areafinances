from django.apps import AppConfig


class FpDataConfig(AppConfig):
    name = 'fp_data'
